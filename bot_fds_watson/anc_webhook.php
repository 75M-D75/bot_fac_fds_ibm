<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>WatsoneBot</title>
</head>
<body>
  <div>
  <p>
  <?php 

    function create_session() {
      //echo "Creation de session ";
      saut_line();
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); 
      $ret = curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANYSAFE | CURLAUTH_ANY);
      //echo " CURLOPT_HTTPAUTH = $ret</br>";
      $ret = curl_setopt($ch, CURLOPT_USERPWD, "apikey:DkykETpsYHi4SY_EE6bwMXI8NLx-duD6AtzSH7B0u7Qo");
      //echo " CURLOPT_USERPWD = $ret</br>";
      $ret = curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
      //echo " CURLOPT_CUSTOMREQUEST = $ret</br>";
      $ret = curl_setopt($ch, CURLOPT_URL, "https://gateway-syd.watsonplatform.net/assistant/api/v2/assistants/ceee8af1-5ea8-4653-a0bf-c1bcd578132a/sessions?version=2018-11-08");
      //echo " CURLOPT_URL = $ret</br>";
      $ret = curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
      //echo "CURLOPT_RETURNTRANSFER = $ret</br>";
      $res = curl_exec($ch);
      /*if($res == false)
        echo "Execution Fail";
      else
        echo "Execution url OK";*/

      //echo "</br> resultats = $res </br>";
      //echo json_decode($res, true);
      // fermeture des ressources
      curl_close($ch);
      $decoded = json_decode($res, true);
      //echo $decoded["session_id"];

      saut_line();
      //echo "Fin de Creation de session ";
      saut_line();

      return $decoded["session_id"];
    }

    function lecture_message($message, $sessions_id){
      saut_line();
      //echo "Lecture_message ";
      saut_line();
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      $ret = curl_setopt($ch, CURLOPT_POST, true);
      //echo " CURLOPT_POST = $ret</br>";
      $ret = curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANYSAFE | CURLAUTH_ANY);
      //echo " CURLOPT_HTTPAUTH = $ret</br>";
      // configuration des options
      $ret = curl_setopt($ch, CURLOPT_USERPWD, "apikey:DkykETpsYHi4SY_EE6bwMXI8NLx-duD6AtzSH7B0u7Qo");
      //echo " CURLOPT_USERPWD = $ret</br>";
      $ret = curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
      //echo " CURLOPT_CUSTOMREQUEST = $ret</br>";
      $ret = curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type:application/json"));
      //echo " CURLOPT_HTTPHEADER = $ret</br>";
      $data = "{\"input\": {\"text\": \"$message\"}}";
      //echo "data == $data</br>";
      $ret = curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
      //echo " CURLOPT_POSTFIELDS = $ret</br>";
      $ret = curl_setopt($ch, CURLOPT_URL, "https://gateway-syd.watsonplatform.net/assistant/api/v2/assistants/ceee8af1-5ea8-4653-a0bf-c1bcd578132a/sessions/".$sessions_id."/message?version=2018-11-08");
      //echo " CURLOPT_URL = $ret</br>";
      $ret = curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
      //echo "CURLOPT_RETURNTRANSFER = $ret</br>";
      //exécution de la session
      $res = curl_exec($ch);
      if ($res === FALSE) {
          die('Erreur curl_exec(): ' . curl_error($ch));
      }
      $update = json_decode($res, true);
      saut_line();
      //echo "update ";
      if(array_key_exists('code', $update)){
        //echo $update["code"]." -- ";
        //echo $update["code"] == "405";
        //echo "  ...  ";
      }
      
      //echo 2 == 2;
      saut_line();
      //print_r($update);
      if( !array_key_exists('error', $update) && (!array_key_exists('code', $update) || $update["code"] != "404") ){
        //echo "</br>up = ".$update;
        //echo "</br>genere = ".$update["output"]["generic"][0]["text"]." -- ".$update["output"]["intents"][0]["intent"];
        //echo "</br> --".$res."-- </br>";
        //echo $res;
        // fermeture des ressources
        curl_close($ch);
      }
      saut_line();
      //echo "Fin lecture_message ";
      saut_line();
      return $update;

    }

    function saut_line(){
      echo "</br>";
    }

    // fonction qui envoie un message à l'utilisateur
    function sendMessage($chat_id, $text) {
      $q = http_build_query([
          'chat_id' => $chat_id,
          'text' => $text
          ]);
      file_get_contents('https://api.telegram.org/bot'.TOKEN.'/sendMessage?'.$q);
    }

    function number_emot_uni($num){
      switch ($num) {
        case 0:
          return "0️⃣";
          break;
        case 1:
          return "1️⃣";
          break;
        case 2:
          return "2️⃣";
          break;
         case 3:
          return "3️⃣";
          break;
         case 4:
          return "4️⃣";
          break;
         case 5:
          return "5️⃣";
          break;
         case 6:
          return "6️⃣";
          break;
         case 7:
          return "7️⃣";
          break;
         case 8:
          return "8️⃣";
          break;
         case 9:
          return "9️⃣";
        default:
          return "0️⃣";
      }
    }

    function donne_heure_in_emot(){
      date_default_timezone_set ( "Europe/Paris" );
      $dat = date('H\:i');
      $date = "";

      for ($i = 0; $i<strlen($dat); $i++) {
        if ($dat[$i] != ':'){
          //echo $dat[$i] . " \ ";
          $date .= number_emot((int)$dat[$i]);
        }
        else{
          $date .= $dat[$i]."";      
        }
      }

      return $date;
    }
  ?>

  <?php 
    function executer_tri_rapide ($debut, $fin) {
        if ($debut < $fin) {
            $indice_pivot = partitionner_2($debut, $fin);
            executer_tri_rapide($debut, $indice_pivot - 1);
            executer_tri_rapide($indice_pivot + 1, $fin);
        }
    }
     
    function partitionner_2 ($debut, $fin) {
        global $t;
        global $locaTableau;
        global $matchTableau;
        global $descTableau;
        global $dateEndTableau;

        $valeur_pivot       = $t[$debut];
        $valeur_pivot_loc   = $locaTableau[0][$debut];
        $valeur_pivot_match = $matchTableau[0][$debut];
        $valeur_pivot_des   = $descTableau[0][$debut];
        $valeur_pivot_dateEnd = $dateEndTableau[0][$debut];
        $d = $debut + 1;
        $f = $fin;
        while ($d < $f) {
            while ($d < $f && $t[$f] >= $valeur_pivot) $f--;
            while ($d < $f && $t[$d] <= $valeur_pivot) $d++;
            $t = echange($t, $d, $f);
            $locaTableau [0] = echange($locaTableau[0],  $d, $f);
            $matchTableau[0] = echange($matchTableau[0], $d, $f);
            $descTableau [0] = echange($descTableau[0],  $d, $f);
            $dateEndTableau [0] = echange($dateEndTableau[0],  $d, $f);
        }
        if ($t[$d] > $valeur_pivot) $d--;
        $t[$debut] = $t[$d];
        $t[$d] = $valeur_pivot;

        $locaTableau[0][$debut] = $locaTableau[0][$d];
        $locaTableau[0][$d] = $valeur_pivot_loc;

        $matchTableau[0][$debut] = $matchTableau[0][$d];
        $matchTableau[0][$d] = $valeur_pivot_match; 

        $descTableau[0][$debut] = $descTableau[0][$d];
        $descTableau[0][$d] = $valeur_pivot_des;  

        $dateEndTableau[0][$debut] = $dateEndTableau[0][$d];
        $dateEndTableau[0][$d] = $valeur_pivot_dateEnd;       
        return $d;
    }

    function appel_calendrier(){
      global $calendrier;
      global $dateTableau;
      global $n;
      global $locaTableau ;
      global $matchTableau;
      global $descTableau ;
      global $dateEndTableau;

      // Expressions régulières
      $regExpMatch   = '/SUMMARY:(.*)/';
      $regExpDate    = '/DTSTART:(.*)/';
      $regExpDateEnd = '/DTEND:(.*)/';
      $regExpDesc    = '/DESCRIPTION:(.*)/';
      $regExpLocat   = '/LOCATION:(.*)/';

      $calendrier = file_get_contents('https://proseconsult.umontpellier.fr/jsp/custom/modules/plannings/direct_cal.jsp?data=5e3670a1af6484011850addbbf026abb1801c9e8db0d8cf6680e09872cce84f9e0fa50826f0818af16cfc8af7aef7fd1906f45af276f59aec18424f8595af9f9a6b28cb855546dc71e4c0e6e6a459e378d3f4109b6629391');

      $n = preg_match_all($regExpMatch, $calendrier, $matchTableau, PREG_PATTERN_ORDER);
      preg_match_all($regExpDate, $calendrier, $dateTableau, PREG_PATTERN_ORDER);
      preg_match_all($regExpDateEnd, $calendrier, $dateEndTableau, PREG_PATTERN_ORDER);
      preg_match_all($regExpDesc, $calendrier, $descTableau, PREG_PATTERN_ORDER);
      preg_match_all($regExpLocat, $calendrier, $locaTableau, PREG_PATTERN_ORDER);
    }

    function echange($T, $i, $j){
      $temp  = $T[$i];
      $T[$i] = $T[$j];
      $T[$j] = $temp;

      return $T;
    }

    function num_day($day){
      switch ($day) {

        case 'Mon':
          # code...
          return 0;
          break;

        case 'Tue':
          # code...
          return 1;
          break;
        case 'Wed':
          # code...
          return 2;
          break;

        case 'Thu':
          # code...
          return 3;
          break;

        case 'Fri':
          # code...
          return 4;
          break;
        
        default:
          # code...
          return -1;
          break;
      }
    }


    function day_num($num){
      switch ($num) {

        case 0:
          # code...
          return 'Lundi';
          break;

        case 1:
          # code...
          return 'Mardi';
          break;
        case 2:
          # code...
          return 'Mercredi';
          break;

        case 3:
          # code...
          return 'Jeudi';
          break;

        case 4:
          # code...
          return 'Vendredi';
          break;
        
        default:
          # code...
          return -1;
          break;
      }
    }

    /*function date_inferieur($date_1, $date_2){
      $annee_1 = substr($date_1, 8, 4);
      $mois_1  = substr($date_1, 12, 2);
      $jour_1  = substr($date_1, 14, 2);

      $annee_2 = substr($date_2, 8, 4);
      $mois_2  = substr($date_2, 12, 2);
      $jour_2  = substr($date_2, 14, 2);
      //echo $jour_1.'/'.$mois_1.'/'.$annee_1." --<br>";
      //echo $jour_2.'/'.$mois_2.'/'.$annee_2." --<br>";

      if( $annee_1 < $annee_2 ){
        return -1;
      }
      else if($annee_1 == $annee_2){
        if( $mois_1 < $mois_2 ){
          return -1;
        }
        else if( $mois_1 == $mois_2 ){
          if( $jour_1 < $jour_2 ){
            return -1;
          }
          else if( $jour_1 == $jour_2 ){
            return 0;
          }
          else{
            return 1;
          }
        }
        else{
          return 1;
        }
      }
      else{
        return 1;;
      }
    }*/
  ?>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
  <script> 

    var c ="okkkk";
    function recup_test(){
      console.log("dep");
      url = "http://api.giphy.com/v1/gifs/search";
      url += "?q=funny+cat&api_key=qwNLtV6LgoHnfRkV1RPWHnRPJhRwad0X";

      const req = new XMLHttpRequest();
      req.open('GET', url, false); 
      req.send(null);
      var reponse = " null ";

      if (req.status === 200) {
          console.log("Réponse reçue: %s", req.responseText);
          k = JSON.parse(req.responseText);
          console.log("val: %s", k.data[0]);
          console.log(k.data[0].images.original.url);
          reponse = k.data[0].images.original.url;
      }
      else {
          console.log("Status de la réponse: %d (%s)", req.status, req.statusText);
      }

      $.post("webhook.php", {data:reponse}, function(data){

          
      },"json");
      return reponse;
    }

    //recup_test();
  </script>

  <?php
    echo "<script>alert('bb'); recup_test();</script>";
    echo "yml";
    saut_line();
    //echo "<script> recup_test(); </script>";
    echo $_POST["data"];
    saut_line();
    $jour = 29;
    $annee = 2019;
    $mois = 01;
     
    $timestamp = mktime(0, 0, 0, $mois, $jour, $annee); //Donne le timestamp correspondant à cette date
    //echo $mois."-".$jour."-".$annee."<br>";
    //echo date('d-m-Y', $timestamp)."<br>";
    $timestamp_2 = strtotime(date('d-m-Y', $timestamp));
    //echo $timestamp."<br>";
    //echo $timestamp_2."<br>";
    $nbJoursTimestamp = ($timestamp - $timestamp_2);
    //echo ($timestamp - $timestamp_2)."<br>";
    $nbJours = $nbJoursTimestamp/86400;
    //echo $nbJours."<br>";
    //echo date('D', $timestamp)."<br>";
    //qwNLtV6LgoHnfRkV1RPWHnRPJhRwad0X




    date_default_timezone_set ( "Europe/Paris" );
    //echo date('H\:i');
    saut_line();
    //echo "Date : 15/01/19 12 38";
    saut_line();
    $message = NULL;
    $chat_id = NULL;
    define('TOKEN', '753741907:AAGlLfZ5m6bwoyj8QGYxkSA3SvDBNCh39FE');

    $content = file_get_contents('message.txt');
    //$content = file_get_contents('php://input');
    $update = json_decode($content, true);

    $message = $update['message']['text'];
    $chat_id = $update['message']['chat']['id'];


    echo "Contenu : ".$content;
    saut_line();
    //echo "Message : ".$message;
    saut_line();
    //echo "Chat id : ".$chat_id;

    $tab = explode ( " ", $message );
    saut_line();

    //watsonBCDBot
    session_start();

    $regWatson = '/([Ww][aA][Tt][Ss][Oo][Nn])/';

    if( preg_match_all($regWatson, $message) || true ){
      //echo "-- Reponse";
      saut_line();
      // repondre
      // initialisation de la session
      if( array_key_exists('session_id_api', $_SESSION) && $_SESSION["session_id_api"] != null){
        //$session_id = $_SESSION["session_id_api"];
        //echo "valiiiiiiiiiiiiiiiiiiide ";
        saut_line();
      }

      saut_line();
      $session_id = file_get_contents('session.txt');
      //echo "ancienne sessionid = ".$session_id;

      saut_line();
      if( !isset($session_id) || $session_id == NULL ){
        $session_id = create_session();
        $_SESSION["session_id_api"] = $session_id;
        file_put_contents('session.txt', $session_id);
        saut_line();
        //echo $session_id." session_id_api ";
        saut_line();
        //echo "oooooooooooooooooooooooooooooooooooooooo ---------------- 0";
        saut_line();
        saut_line();
      }
      

      $update = lecture_message($message, $session_id);
      saut_line();
      $nb_error = 0;
      while( (array_key_exists('error', $update) || (array_key_exists('code', $update) && $update["code"] == "404")) && $nb_error < 10  ){
        $session_id = create_session();
        $_SESSION["session_id_api"] = $session_id;
        file_put_contents('session.txt', $session_id);
        $update = lecture_message($message, $session_id);
        saut_line();
        $nb_error = $nb_error + 1;
      }

      if( !array_key_exists('error', $update) ){
        $message_send = $update["output"]["generic"][0]["text"];
        $intent = $update["output"]["intents"][0]["intent"];
        $message_send .= " : -- ".$intent;

        if ($intent == "Heure") {
          $message_send = date('H\:i');
        }
        else if( $intent == 'Emploi_Du_Temps' ){
          $message_send = "";
          date_default_timezone_set ( "Europe/Paris" );
          $dat  = date('d-m-o');
          $time = date('H\hi');
          $heure_now = date('H');
          $minut = date('i');

          $calendrier = null;
          $n = null;
          $dateTableau = null;
          $locaTableau = null;
          $matchTableau= null;
          $descTableau = null;
          $dateEndTableau = null;

          appel_calendrier();

          $t = $dateTableau[0];    // le tableau est une variable globale

          executer_tri_rapide(0, sizeof($t)-1);
          $dateTableau[0] = $t;

          $date_precedente = null;
          $compt = 0;
          for ($j=0 ; $j < $n && $compt<7 ; ++$j)
          {
            $annee = substr($dateTableau[0][$j], 8, 4);
            $mois  = substr($dateTableau[0][$j], 12, 2);
            $jour  = substr($dateTableau[0][$j], 14, 2);
            $heure = substr($dateTableau[0][$j], 17, 2);
            $min   = substr($dateTableau[0][$j], 19, 2);

            $heure_end = substr($dateEndTableau[0][$j], 17-2, 2);
            $min_end   = substr($dateEndTableau[0][$j], 19-2, 2);

            $match = substr($matchTableau[0][$j], 8);
            $desc  = substr($descTableau[0][$j], 12);
            $date  = $jour.'-'.$mois.'-'.$annee;
            $horaire = $heure.'h'.$min;

            $horaire_end = $heure_end.'h'.$min_end;
            $location = substr($locaTableau[0][$j], 9);
            $location_2 = explode('\,', $location);
            $location = "";

            ////echo $time." <br> ";

            for ($i=0; $i<sizeof($location_2); $i++) {
              $location .= $location_2[$i]."  ";
            }

            $timestamp = mktime(0, 0, 0, $mois, $jour, $annee); //Donne le timestamp correspondant à cette date
            $jour_selon_date = date('D', $timestamp);

            // Affichage
            if( ( strtotime($date) - strtotime($dat) >= 0 && (int)$heure >= (int)$heure_now ) || (strtotime($date) - strtotime($dat)) > 0  ){
              if($date_precedente != null && $date_precedente == $date){
                //echo "  ".$horaire." à ".$horaire_end."<br>  ".$match."<br>  Salle : ".$location." <br><br>";

                $message_send .= "   ".$horaire." à ".$horaire_end."\n   ".$match."\n   Salle : ".$location." \n\n";
              }
              else{
                //echo day_num(num_day(date('D' ,$timestamp)))." ".date('d' ,$timestamp)." <br>  ".$horaire." à ".$horaire_end."<br>  ".$match."<br>  Salle : ".$location." <br><br>";

                $message_send .= day_num(num_day(date('D' ,$timestamp)))." ".date('d' ,$timestamp)." :\n   ".$horaire." à ".$horaire_end."\n   ".$match."\n   Salle : ".$location." \n\n";
                $date_precedente = $date;
              }
              $compt++;
            }
          }
        }
        $tab = explode ( " ", $message );
        saut_line();
        //echo " tab 1 = $tab[0],     $tab[1]";
        //echo $message_send; 
        //sendMessage($chat_id, $message_send);
      }
    }
  ?>
  </p>
  </div>
</body>
</html>


