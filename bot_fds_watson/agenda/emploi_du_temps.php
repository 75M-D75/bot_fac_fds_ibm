<?php 
    function executer_tri_rapide ($debut, $fin) {
        if ($debut < $fin) {
            $indice_pivot = partitionner_2($debut, $fin);
            executer_tri_rapide($debut, $indice_pivot - 1);
            executer_tri_rapide($indice_pivot + 1, $fin);
        }
    }
     
    function partitionner_2 ($debut, $fin) {
        global $t;
        global $locaTableau;
        global $matchTableau;
        global $descTableau;
        global $dateEndTableau;

        $valeur_pivot       = $t[$debut];
        $valeur_pivot_loc   = $locaTableau[0][$debut];
        $valeur_pivot_match = $matchTableau[0][$debut];
        $valeur_pivot_des   = $descTableau[0][$debut];
        $valeur_pivot_dateEnd = $dateEndTableau[0][$debut];
        $d = $debut + 1;
        $f = $fin;
        while ($d < $f) {
            while ($d < $f && $t[$f] >= $valeur_pivot) $f--;
            while ($d < $f && $t[$d] <= $valeur_pivot) $d++;
            $t = echange($t, $d, $f);
            $locaTableau [0] = echange($locaTableau[0],  $d, $f);
            $matchTableau[0] = echange($matchTableau[0], $d, $f);
            $descTableau [0] = echange($descTableau[0],  $d, $f);
            $dateEndTableau [0] = echange($dateEndTableau[0],  $d, $f);
        }
        if ($t[$d] > $valeur_pivot) $d--;
        $t[$debut] = $t[$d];
        $t[$d]     = $valeur_pivot;

        $locaTableau[0][$debut]     = $locaTableau[0][$d];
        $locaTableau[0][$d]         = $valeur_pivot_loc;

        $matchTableau[0][$debut]    = $matchTableau[0][$d];
        $matchTableau[0][$d]        = $valeur_pivot_match; 

        $descTableau[0][$debut]     = $descTableau[0][$d];
        $descTableau[0][$d]         = $valeur_pivot_des;  

        $dateEndTableau[0][$debut]  = $dateEndTableau[0][$d];
        $dateEndTableau[0][$d]      = $valeur_pivot_dateEnd;       
        return $d;
    }

    function appel_calendrier(){
      global $calendrier;
      global $dateTableau;
      global $n;
      global $locaTableau ;
      global $matchTableau;
      global $descTableau ;
      global $dateEndTableau;

      // Expressions régulières
      $regExpMatch    = '/SUMMARY:(.*)/';
      $regExpDate     = '/DTSTART:(.*)/';
      $regExpDateEnd  = '/DTEND:(.*)/';
      $regExpDesc     = '/DESCRIPTION:(.*)/';
      $regExpLocat    = '/LOCATION:(.*)/';

      //lien par default à changer selon les besoins
      $lien = 'https://proseconsult.umontpellier.fr/jsp/custom/modules/plannings/direct_cal.jsp?data=5e3670a1af6484011850addbbf026abb1801c9e8db0d8cf6680e09872cce84f9e0fa50826f0818afd07cb68a5f59ac56906f45af276f59ae8fac93f781e86152aa9968683a1f104985a0a3f75ee8b61ec2973627c2eb073b718a4b583f9c700ce7a16f21e9bcc97b065ac65755642861602188274917d1295e6b0c7892c179d0183e7f7d3560e47902bf7324b465b5ca784f2a75df7d8f2dc0583b6a2e55bb8bcc0de10b3cc5f0d8c4c474f1d57c6d6bea11e5e549d13dea87a877d67d313086c87acba77c67405b0ede1deea71c36f7311a4784670af693a70c6c5d179d8b4ed2e886740e0b72c11567b421dd4626e7420d07a6f80edd19';
      
      if( array_key_exists('lien', $_POST) ){
        //echo "ok";
        $lien = $_POST['lien'];
      }

      $calendrier     = file_get_contents($lien);

      $n = preg_match_all($regExpMatch, $calendrier, $matchTableau, PREG_PATTERN_ORDER);
      preg_match_all($regExpDate, $calendrier, $dateTableau, PREG_PATTERN_ORDER);
      preg_match_all($regExpDateEnd, $calendrier, $dateEndTableau, PREG_PATTERN_ORDER);
      preg_match_all($regExpDesc, $calendrier, $descTableau, PREG_PATTERN_ORDER);
      preg_match_all($regExpLocat, $calendrier, $locaTableau, PREG_PATTERN_ORDER);
    }

    function echange($T, $i, $j){
      $temp  = $T[$i];
      $T[$i] = $T[$j];
      $T[$j] = $temp;

      return $T;
    }

    function num_day($day){
      switch ($day) {

        case 'Mon':
          # code...
          return 0;
          break;

        case 'Tue':
          # code...
          return 1;
          break;
        case 'Wed':
          # code...
          return 2;
          break;

        case 'Thu':
          # code...
          return 3;
          break;

        case 'Fri':
          # code...
          return 4;
          break;
        
        default:
          # code...
          return -1;
          break;
      }
    }

    date_default_timezone_set ( "Europe/Paris" );
    $dat_actuelle   = date('d/m/o');
    $time           = date('H\hi');
    $heure_now      = date('H');
    $minut          = date('i');
    //echo $dat_actuelle." ".$heure_now.":".$minut;

    $calendrier     = null;
    $n              = null;
    $dateTableau    = null;
    $locaTableau    = null;
    $matchTableau   = null;
    $descTableau    = null;
    $dateEndTableau = null;

    appel_calendrier();

    $t = $dateTableau[0];    // le tableau est une variable globale

    executer_tri_rapide(0, sizeof($t)-1);
    $dateTableau[0] = $t;


    $retour = array();
    $indice_results = 0;

    $retour['results']['Mon'] = null;
    $retour['results']['Tue'] = null;
    $retour['results']['Wed'] = null;
    $retour['results']['Thu'] = null;
    $retour['results']['Fri'] = null;

    $retour['results'][num_day('Mon')] = null;
    $retour['results'][num_day('Tue')] = null;
    $retour['results'][num_day('Wed')] = null;
    $retour['results'][num_day('Thu')] = null;
    $retour['results'][num_day('Fri')] = null;

    $jour_enr     = null;
    $jour_restant = 7;
    $dureesejour  = 0;
    $date1        = null;
    $nbJours      = 0;

    $recup_date_init    = false;
    $date_lundi_week    = null;
    $chaine_next_lundi  = "";

    //True pour rajouter une heure de decalage sinon false depend de la periode de l'annee
    $decalage = true;


    for ($j=0 ; $j < $n && $jour_restant >= $nbJours; ++$j)
    {
      $annee        = substr($dateTableau[0][$j], 8, 4);
      $mois         = substr($dateTableau[0][$j], 12, 2);
      $jour         = substr($dateTableau[0][$j], 14, 2);
      $heure        = substr($dateTableau[0][$j], 17, 2);
      $min          = substr($dateTableau[0][$j], 19, 2);

      $heure_end    = substr($dateEndTableau[0][$j], 17-2, 2);
      $min_end      = substr($dateEndTableau[0][$j], 19-2, 2);

      $match        = substr($matchTableau[0][$j], 8);
      $desc         = substr($descTableau[0][$j], 12);
      $date         = $mois.'/'.$jour.'/'.$annee;
      $horaire      = $heure.'h'.$min;
      $location     = substr($locaTableau[0][$j], 9);
      $location_2   = explode('\,', $location);
      $location     = "";

      //gestion du decalage horaire
      if( $decalage ){
        $heure        = "".((int)$heure+1);
        $heure_end    = "".((int)$heure_end+1);
      }

      //-------------------- Debug -----------------------
      //echo "<br>// /// --- ".$n." ";
      //echo $annee." ".$mois." ".$jour." ".$heure." ".$min." ".$desc;
      //printr($location_2);
      //echo $time." <br> ";

      //separer les salles
      for ($i=0; $i<sizeof($location_2); $i++) {
        if($i+1<sizeof($location_2))
            $location .= $location_2[$i]." ou ";
        else
            $location .= $location_2[$i]."  ";
      }

      //-------------------- Debug -----------------------
      //echo date('d-m-Y',strtotime('-1 Monday'))." ".date('d-m-Y',strtotime('Tuesday'))." ";

      //recupe lundi de la semaine
      
      if($recup_date_init == false){
        //-------------------- Debug -----------------------
        //echo date('d-m-Y', strtotime('Monday'))." -- ".date('d-m-Y', strtotime("now"))."<br/>";

        if( strtotime('Monday') - strtotime("now") > 0 ){
          $date_lundi_week = strtotime('-1 Monday');
          $chaine_next_lundi = "Monday";
        }
        else{
          $date_lundi_week = strtotime('Monday');
          $chaine_next_lundi = "+1 Monday";
          if( date('m/d/Y', strtotime("+1 Monday")) == date('m/d/Y', strtotime("Monday"))){
            $chaine_next_lundi = "+2 Monday";
          }
        }
      }

      //-------------------- Debug -----------------------
      //echo date('d-m-Y', $date_lundi_week)." :: ";

      //
      if( $recup_date_init == false && 
        ( ( date('d-m-Y',$date_lundi_week) == date('d-m-Y', strtotime("now")) && date('d-m-Y', $date_lundi_week) == date('d-m-Y', strtotime($date)) )  || ( (strtotime($date) - strtotime("now") >= 0) ) ) ){
        $timestamp = mktime(0, 0, 0, $mois, $jour, $annee); //Donne le timestamp correspondant à cette date
     
        $jour_selon_date  = date('D', $timestamp);
        $jour_restant     = $jour_restant - num_day($jour_selon_date);

        $date1            = date('m/d/Y', $timestamp);
        $recup_date_init  = true;
        //echo date('d-m-Y', strtotime($date))." ".$mois." j ".$jour." a ".$annee."<br/>";
        //echo "<br/>";
        //echo date('d-m-Y',$date_lundi_week)." -- ".date('d-m-Y', strtotime("now"));
        //echo "<br/>";
        //-------------------- Debug -----------------------
        //echo $date1;
        //echo " string<br/>";
      }
      
      //-------------------- Debug -----------------------
      //echo date('d-m-Y', strtotime($date))." -- ".date('d-m-Y', strtotime("now"))."<br/>";
      //echo ".<br/>";
      //echo date("d-m-Y", strtotime("Monday"));
/*      if($recup_date_init == true){
        echo date('m/d/Y', strtotime("+1 Monday")). " " ;
      }*/
      if((strtotime($date) - strtotime($chaine_next_lundi) > 0)){
        break;
      }

      // Affichage
      if( ( ( ( strtotime($date) - $date_lundi_week >= 0) && (strtotime($date) - strtotime($chaine_next_lundi) < 0) ) ) && $recup_date_init ){
        //-------------------- Debug -----------------------
        //echo date('d-m-Y', strtotime($date))." -- ".date('d-m-Y', strtotime("now"))."<br/>";
        //echo date('d-m-Y', $date_lundi_week)." !!! ".date('d-m-Y', strtotime("now"))."<br/>";


        $timestamp        = mktime(0, 0, 0, $mois, $jour, $annee); //Donne le timestamp correspondant à cette date
        $jour_selon_date  = date('D', $timestamp);

        $date2            = date('m/d/Y', $timestamp); // date fr exemple le 01 octobre 2010

        // On transforme les 2 dates en timestamp
        $date1_timestamp  = strtotime($date1);
        $date2_timestamp  = strtotime($date2);
         
        // On récupère la différence de timestamp entre les 2 précédents
        $nbJoursTimestamp = abs($date2_timestamp - $date1_timestamp);
         
        // ** Pour convertir le timestamp (exprimé en secondes) en jours **
        // On sait que 1 heure = 60 secondes * 60 minutes et que 1 jour = 24 heures donc :
        $nbJours          = $nbJoursTimestamp/86400; // 86 400 = 60*60*24

        //
        if($jour_enr != $jour_selon_date){
          if($jour_enr != null){
            $indice_results = 0;
          }
          $jour_enr = $jour_selon_date;
        }
        
        if( $jour_restant >= $nbJours || true ){

          //-------------------- Debug -----------------------
          //echo $date." ; ".$horaire." ; ".$match." ; ".$location." <br>";
          //echo num_day($jour_selon_date)." ".$jour_selon_date." ";

          $retour['results'][num_day($jour_selon_date)]['date'][$indice_results]       = $date;
          $retour['results'][num_day($jour_selon_date)]['match'][$indice_results]      = $match;
          $retour['results'][num_day($jour_selon_date)]['location'][$indice_results]   = $location;
          $retour['results'][num_day($jour_selon_date)]['heure'][$indice_results]      = ((int)$heure).':'.$min;
          $retour['results'][num_day($jour_selon_date)]['heure_end'][$indice_results]  = ((int)$heure_end).':'.$min_end;
          
          $indice_results++;
        }
      }
    }

    echo json_encode($retour);
?>
