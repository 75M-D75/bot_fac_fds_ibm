
<!DOCTYPE html>
<html lang="en" class="no-js">

<!--  PHP : initialise la page selon une requete GET url  -->
<?php
    if( array_key_exists('lien_edt', $_GET) ){
        # Our new data
        echo "
            <script type=\"text/javascript\">
            var lien_edt = "."\"".$_GET["lien_edt"]."\"".";
            //alert(lien_edt);
            </script>
        ";
    }
    else{
        echo "
            <script type=\"text/javascript\">
            var lien_edt = null;
            //alert('ko');
            </script>
        ";
    }
?>

<?php
    if( array_key_exists('profile', $_GET) ){
        # Our new data
        echo "
            <script type=\"text/javascript\">
            var profile = "."\"".$_GET["profile"]."\"".";
            //alert(lien_edt);
            </script>
        ";
    }
    else{
        echo "
            <script type=\"text/javascript\">
            var profile = null;
            //alert('ko');
            </script>
        ";
    }
?>


<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600" rel="stylesheet">
	<link rel="stylesheet" href="css/reset.css"> <!-- CSS reset -->
	<link rel="stylesheet" href="css/style.css"> <!-- Resource style -->
  	
	<title>Emploi du temps BCD</title>
	<script src="js/main.js"></script>
</head>
<body >
<div class="cd-schedule loading">
	<div class="timeline">
		<ul>
			<li><span>07:00</span></li>
			<li><span>07:30</span></li>
			<li><span>08:00</span></li>
			<li><span>08:30</span></li>
			<li><span>09:00</span></li>
			<li><span>09:30</span></li>
			<li><span>10:00</span></li>
			<li><span>10:30</span></li>
			<li><span>11:00</span></li>
			<li><span>11:30</span></li>
			<li><span>12:00</span></li>
			<li><span>12:30</span></li>
			<li><span>13:00</span></li>
			<li><span>13:30</span></li>
			<li><span>14:00</span></li>
			<li><span>14:30</span></li>
			<li><span>15:00</span></li>
			<li><span>15:30</span></li>
			<li><span>16:00</span></li>
			<li><span>16:30</span></li>
			<li><span>17:00</span></li>
			<li><span>17:30</span></li>
			<li><span>18:00</span></li>
		</ul>
	</div> <!-- .timeline -->

	<div class="events">
		<ul>
			<li class="events-group">
				<div class="top-info"><span>Lundi</span></div>

				<ul>
					<li class="single-event" data-start="09:30" data-end="10:30" data-content="event-abs-circuit" data-event="event-1">
						<a href="#0">
							<em class="event-name">Abs Circuit</em>
						</a>
					</li>

					<li class="single-event" data-start="11:00" data-end="12:30" data-content="event-rowing-workout" data-event="event-2">
						<a href="#0">
							<em class="event-name">Rowing Workout</em>
						</a>
					</li>

					<li class="single-event" data-start="14:00" data-end="15:15"  data-content="event-yoga-1" data-event="event-3">
						<a href="#0">
							<em class="event-name">Yoga Level 1</em>
						</a>
					</li>
				</ul>
			</li>

			<li class="events-group">
				<div class="top-info"><span>Mardi</span></div>

				<ul>
					<li class="single-event" data-start="10:00" data-end="11:00"  data-content="event-rowing-workout" data-event="event-2">
						<a href="#0">
							<em class="event-name">Rowing Workout</em>
						</a>
					</li>

					<li class="single-event" data-start="11:30" data-end="13:00"  data-content="event-restorative-yoga" data-event="event-4">
						<a href="#0">
							<em class="event-name">Restorative Yoga</em>
						</a>
					</li>

					<li class="single-event" data-start="13:30" data-end="15:00" data-content="event-abs-circuit" data-event="event-1">
						<a href="#0">
							<em class="event-name">Abs Circuit</em>
						</a>
					</li>

					<li class="single-event" data-start="15:45" data-end="16:45"  data-content="event-yoga-1" data-event="event-3">
						<a href="#0">
							<em class="event-name">Yoga Level 1</em>
						</a>
					</li>
				</ul>
			</li>

			<li class="events-group">
				<div class="top-info"><span>Mercredi</span></div>

				<ul>
					<li class="single-event" data-start="09:00" data-end="10:15" data-content="event-restorative-yoga" data-event="event-4">
						<a href="#0">
							<em class="event-name">Restorative Yoga</em>
						</a>
					</li>

					<li class="single-event" data-start="10:45" data-end="11:45" data-content="event-yoga-1" data-event="event-3">
						<a href="#0">
							<em class="event-name">Yoga Level 1</em>
						</a>
					</li>

					<li class="single-event" data-start="12:00" data-end="13:45"  data-content="event-rowing-workout" data-event="event-2">
						<a href="#0">
							<em class="event-name">Rowing Workout</em>
						</a>
					</li>

					<li class="single-event" data-start="13:45" data-end="15:00" data-content="event-yoga-1" data-event="event-3">
						<a href="#0">
							<em class="event-name">Yoga Level 1</em>
						</a>
					</li>
				</ul>
			</li>

			<li class="events-group">
				<div class="top-info"><span>Jeudi</span></div>

				<ul>
					<li class="single-event" data-start="09:30" data-end="10:30" data-content="event-abs-circuit" data-event="event-1">
						<a href="#0">
							<em class="event-name">Abs Circuit</em>
						</a>
					</li>

					<li class="single-event" data-start="12:00" data-end="13:45" data-content="event-restorative-yoga" data-event="event-4">
						<a href="#0">
							<em class="event-name">Restorative Yoga</em>
						</a>
					</li>

					<li class="single-event" data-start="15:30" data-end="16:30" data-content="event-abs-circuit" data-event="event-1">
						<a href="#0">
							<em class="event-name">Abs Circuit</em>
						</a>
					</li>

					<li class="single-event" data-start="17:00" data-end="18:30"  data-content="event-rowing-workout" data-event="event-2">
						<a href="#0">
							<em class="event-name">Rowing Workout</em>
						</a>
					</li>
				</ul>
			</li>

			<li class="events-group">
				<div class="top-info"><span>Vendredi</span></div>

				<ul>
					<li class="single-event" data-start="10:00" data-end="11:00"  data-content="event-rowing-workout" data-event="event-2">
						<a href="#0">
							<em class="event-name">Rowing Workout</em>
						</a>
					</li>

					<li class="single-event" data-start="12:30" data-end="14:00" data-content="event-abs-circuit" data-event="event-1">
						<a href="#0">
							<em class="event-name">Abs Circuit</em>
						</a>
					</li>

					<li class="single-event" data-start="15:45" data-end="16:45"  data-content="event-yoga-1" data-event="event-3">
						<a href="#0">
							<em class="event-name">Yoga Level 1</em>
						</a>
					</li>
				</ul>
			</li>
		</ul>
	</div>

	<div class="event-modal">
		<header class="header">
			<div class="content">
				<span class="event-date"></span>
				<h3 class="event-name"></h3>
			</div>

			<div class="header-bg"></div>
		</header>

		<div class="body">
			<div class="event-info"></div>
			<div class="body-bg"></div>
		</div>

		<a href="#0" class="close">Close</a>
	</div>

	<div class="cover-layer"></div>

</div> <!-- .cd-schedule -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
<script src="js/modernizr.js"></script>

<script>
	if( !window.jQuery ) document.write('<script src="js/jquery-3.0.0.min.js"><\/script>');
</script>

<script>

	/*function add_cours_li(ul, texte, heure_deb, heure_fin) {
		li = document.createElement("li");
        li.setAttribute("class", "single-event");
        li.setAttribute("data-start", heure_deb);
        li.setAttribute("data-end", heure_fin);
        li.setAttribute("data-event", "event-1");
        //li.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
        placeEvents(li);
        ul.appendChild(li);

        span = document.createElement("span");
        span.setAttribute("class", "event-date");
        span.innerHTML = heure_deb+" - "+heure_fin;

        a = document.createElement("a");
        a.setAttribute("href", "#0");

        a.appendChild(span);
        li.appendChild(a);

        em = document.createElement("em");
        em.setAttribute("class", "event-name");
        em.innerHTML = texte;

        a.appendChild(em);
	}*/

	/*li_group = document.getElementsByClassName("events-group");

	ul = li_group[0].getElementsByTagName("ul")[0];
	add_cours_li(ul, "HMIN15");*/

	/*function get_emploi_du_temps(){
        var url = "emploi_du_temps.php";

		if( lien_edt != null ){
			alert("here");
		    $.post(url, {lien:lien_edt}, function(data){
	        	//alert("begin");
	            retour = data.results;
	            //alert(retour["match"][0]);
	            console.log(retour["match"][0]);
	            li_group = document.getElementsByClassName("events-group");

				ul = li_group[0].getElementsByTagName("ul")[0];
				//add_cours_li(ul, retour["match"][0]);
				add_cours_li(ul, retour["match"][0], retour["heure"][0], retour["heure_end"][0]);
	            
	        },"json");
	    }
	    else{
	    	//lien par default à changer selon les besoins
	    	lien_edt = 'https://proseconsult.umontpellier.fr/jsp/custom/modules/plannings/direct_cal.jsp?data=5e3670a1af6484011850addbbf026abb1801c9e8db0d8cf6680e09872cce84f9e0fa50826f0818afd07cb68a5f59ac56906f45af276f59ae8fac93f781e86152aa9968683a1f104985a0a3f75ee8b61ec2973627c2eb073b718a4b583f9c700ce7a16f21e9bcc97bbea0b7f00057a001e70ad562b2837a4bb1b455555b9e3d60591be70ca4da819776491e8973b9c5199d7f68b55b107896e7b17491ad593144361ffa3a64468c0f17a1de8d8d2ea4dddca338feceddac90fb1fe40b151f3fff5d17f91eaaf3015774b07c32824c55b9f275176b12ce737a6e5eddee4e94f03384555615cb9156a9c4907ea7dda468b3602188274917d1293ab38c93391617d0ddb4016679db66ba7d7d2e890e64432d23ea327991a4ac5a32d40e02b158114a94fcd3e353fd8148b24b5406402709e3a4e35b4cf806ff3a33a68aa199e822a6dcb46569006c4976b3f5a77cf624f15354e88aa81ee537a6d98dd827bcc4fe3153875d810f8bd41e04064209574c4947e7fe6ad4611be66ff1aaa1cad423f915';

	    	$.post(url, {lien:lien_edt}, function(data){
	        	//alert("begin");
	            retour = data.results;
	            //alert(retour["match"][0]);
	            console.log(retour["match"][0]);
	            li_group = document.getElementsByClassName("events-group");

				ul = li_group[0].getElementsByTagName("ul")[0];
				//add_cours_li(ul, retour["match"][0]);
				add_cours_li(ul, retour["match"][0], retour["heure"][0], retour["heure_end"][0]);
	            
	        },"json");
	    }
    }*/

    if( lien_edt == null ){

		//lien par default à changer selon les besoins
    	lien_edt = 'https://proseconsult.umontpellier.fr/jsp/custom/modules/plannings/direct_cal.jsp?data=5e3670a1af6484011850addbbf026abb1801c9e8db0d8cf6680e09872cce84f9e0fa50826f0818afd07cb68a5f59ac56906f45af276f59ae8fac93f781e86152aa9968683a1f104985a0a3f75ee8b61ec2973627c2eb073b718a4b583f9c700ce7a16f21e9bcc97b1f9735981a98a43be6e1e263798d5504334135927792814dfd6213b42f6b4b7d2b689349a64c97c56bcc5f16c3ad981910cce2c77f1239858887e38fb46427b3662c176d77c6c56234a6a351ecf9923c';
    }

    if( profile != null ){
    	if( profile == "M2" || profile == "M2profile1" ){
    		lien_edt = 'https://proseconsult.umontpellier.fr/jsp/custom/modules/plannings/direct_cal.jsp?data=5e3670a1af6484011850addbbf026abb1801c9e8db0d8cf6680e09872cce84f9e0fa50826f0818afd07cb68a5f59ac56906f45af276f59ae8fac93f781e86152aa9968683a1f104985a0a3f75ee8b61ec2973627c2eb073b718a4b583f9c700ce7a16f21e9bcc97b065ac65755642861602188274917d1295e6b0c7892c179d0183e7f7d3560e47902bf7324b465b5ca784f2a75df7d8f2dc0583b6a2e55bb8bcc0de10b3cc5f0d8c4c474f1d57c6d6bea11e5e549d13dea87a877d67d313086c87acba77c67405b0ede1deea71c36f7311a4784670af693a70c6c5d179d8b4ed2e886740e0b72c11567b421dd4626e7420d07a6f80edd19';
    	}
    	else if( profile == "M2profile2" ){
    		lien_edt = 'https://proseconsult.umontpellier.fr/jsp/custom/modules/plannings/direct_cal.jsp?data=5e3670a1af6484011850addbbf026abb1801c9e8db0d8cf6680e09872cce84f9e0fa50826f0818afd07cb68a5f59ac56906f45af276f59ae8fac93f781e86152aa9968683a1f104985a0a3f75ee8b61ec2973627c2eb073b718a4b583f9c700ce7a16f21e9bcc97bbb99a9690dbcaeecfe2d2d2ec53785e5d4f3f24754a0b566fe4086e37cb12fd22a85a71c9fd75b3c4d5fe34f83240dca4f50007f3f4ec77307b5d5bd04be2e8f9f66576767280b3860404a1ac35b3f5c3bdde677b3b3c44df38ded05bc40a74abb685602df69f47ac2d51486d1a4196fbca0c8fc3f919711f5047367000918f0e70ad562b2837a4b2bf78631caf72877';
    	}
    	else if(profile ==  "M2profile3"){
    		lien_edt = 'https://proseconsult.umontpellier.fr/jsp/custom/modules/plannings/direct_cal.jsp?data=5e3670a1af6484011850addbbf026abb1801c9e8db0d8cf6680e09872cce84f9e0fa50826f0818afd07cb68a5f59ac56906f45af276f59ae8fac93f781e86152aa9968683a1f104985a0a3f75ee8b61ec2973627c2eb073b718a4b583f9c700ce7a16f21e9bcc97b7772e64b7d6b58f7cbeae72f5253962a4d693f5bd8e099db1f0c713c90243a1b';
    	}
    	else if(profile == "M1"){
    		lien_edt = 'https://proseconsult.umontpellier.fr/jsp/custom/modules/plannings/direct_cal.jsp?data=5e3670a1af6484011850addbbf026abb1801c9e8db0d8cf6680e09872cce84f9e0fa50826f0818afd07cb68a5f59ac56906f45af276f59ae8fac93f781e86152aa9968683a1f104985a0a3f75ee8b61ec2973627c2eb073b718a4b583f9c700ce7a16f21e9bcc97b1f9735981a98a43be6e1e263798d5504334135927792814dfd6213b42f6b4b7d2b689349a64c97c56bcc5f16c3ad981910cce2c77f1239858887e38fb46427b3662c176d77c6c56234a6a351ecf9923c';
    	}
    }

</script>


<!-- Resource jQuery -->
<script src="js/main.js"></script> 


</body>
</html>