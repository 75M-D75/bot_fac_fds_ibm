<?php
ob_start();
session_start();
?>

  <?php 
    
    function create_session() {
      echo "Creation de session ";
      saut_line();
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); 
      $ret = curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANYSAFE | CURLAUTH_ANY);
      echo " CURLOPT_HTTPAUTH = $ret</br>";
      $ret = curl_setopt($ch, CURLOPT_USERPWD, "apikey:DkykETpsYHi4SY_EE6bwMXI8NLx-duD6AtzSH7B0u7Qo");
      echo " CURLOPT_USERPWD = $ret</br>";
      $ret = curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
      echo " CURLOPT_CUSTOMREQUEST = $ret</br>";
      $ret = curl_setopt($ch, CURLOPT_URL, "https://gateway-syd.watsonplatform.net/assistant/api/v2/assistants/ceee8af1-5ea8-4653-a0bf-c1bcd578132a/sessions?version=2018-11-08");
      echo " CURLOPT_URL = $ret</br>";
      $ret = curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
      echo "CURLOPT_RETURNTRANSFER = $ret</br>";
      $res = curl_exec($ch);
      if($res == false)
        echo "nooooon";
      else
        echo "Exec OK";
      echo "</br> $res </br>";
      // fermeture des ressources
      curl_close($ch);
      $decoded = json_decode($res, true);
      echo $decoded["session_id"];

      saut_line();
      echo "Fin de Creation de session ";
      saut_line();

      return $decoded["session_id"];
    }

    function lecture_message($message, $sessions_id){
      saut_line();
      echo "Lecture_message ";
      saut_line();
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      $ret = curl_setopt($ch, CURLOPT_POST, true);
      echo " CURLOPT_POST = $ret</br>";
      $ret = curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANYSAFE | CURLAUTH_ANY);
      echo " CURLOPT_HTTPAUTH = $ret</br>";
      // configuration des options
      $ret = curl_setopt($ch, CURLOPT_USERPWD, "apikey:DkykETpsYHi4SY_EE6bwMXI8NLx-duD6AtzSH7B0u7Qo");
      echo " CURLOPT_USERPWD = $ret</br>";
      $ret = curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
      echo " CURLOPT_CUSTOMREQUEST = $ret</br>";
      $ret = curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type:application/json"));
      echo " CURLOPT_HTTPHEADER = $ret</br>";
      $data = "{\"input\": {\"text\": \"$message\"}}";
      echo "data == $data</br>";
      $ret = curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
      echo " CURLOPT_POSTFIELDS = $ret</br>";
      $ret = curl_setopt($ch, CURLOPT_URL, "https://gateway-syd.watsonplatform.net/assistant/api/v2/assistants/ceee8af1-5ea8-4653-a0bf-c1bcd578132a/sessions/".$sessions_id."/message?version=2018-11-08");
      echo " CURLOPT_URL = $ret</br>";
      $ret = curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
      echo "CURLOPT_RETURNTRANSFER = $ret</br>";
      //exécution de la session
      $res = curl_exec($ch);
      if ($res === FALSE) {
          die('Erreur curl_exec(): ' . curl_error($ch));
      }
      $update = json_decode($res, true);
      saut_line();
      //echo "update ";
      /*if(array_key_exists('code', $update)){
        echo $update["code"]." -- ";
        echo $update["code"] == "405";
        echo "  ...  ";
      }*/
      echo "update ";
      saut_line();
      print_r($update);
      if( !array_key_exists('error', $update) && (!array_key_exists('code', $update) || $update["code"] != "404") ){
        //echo "</br>up = ".$update;
        //echo "</br>genere = ".$update["output"]["generic"][0]["text"]." -- ".$update["output"]["intents"][0]["intent"];
        //echo "</br> --".$res."-- </br>";
        // fermeture des ressources
        curl_close($ch);
      }
      saut_line();
      echo "Fin lecture_message ";
      saut_line();
      return $update;

    }

    function saut_line(){
      echo "</br>";
    }

    // fonction qui envoie un message à l'utilisateur
    function sendMessage($chat_id, $text) {
      $q = http_build_query([
          'chat_id' => $chat_id,
          'text' => $text
          ]);
      file_get_contents('https://api.telegram.org/bot'.TOKEN.'/sendMessage?'.$q);
    }
    
    function number_emot_uni($num){
      switch ($num) {
        case 0:
          return "0️⃣";
          break;
        case 1:
          return "1️⃣";
          break;
        case 2:
          return "2️⃣";
          break;
         case 3:
          return "3️⃣";
          break;
         case 4:
          return "4️⃣";
          break;
         case 5:
          return "5️⃣";
          break;
         case 6:
          return "6️⃣";
          break;
         case 7:
          return "7️⃣";
          break;
         case 8:
          return "8️⃣";
          break;
         case 9:
          return "9️⃣";
        default:
          return "0️⃣";
      }
    }

    function donne_heure_in_emot(){
      date_default_timezone_set ( "Europe/Paris" );
      $dat = date('H\:i');
      $date = "";

      for ($i = 0; $i<strlen($dat); $i++) {
        if ($dat[$i] != ':'){
          echo $dat[$i] . " \ ";
          $date .= number_emot_uni((int)$dat[$i]);
        }
        else{
          $date .= $dat[$i]."";      
        }
      }
      
      return $date;
    }
  ?>

  <?php
    function recup_gif($q, $apikey) {
      $arg = http_build_query([
        'q' => $q,
        'apikey' => $apikey
      ]);

      $reponse = file_get_contents('http://api.giphy.com/v1/gifs/search?'.$arg);

      $json_reponse = json_decode($reponse, true);

      //echo " GiF : ".$j["data"][0]["images"]["original"]["url"];
      return $json_reponse["data"][0]["images"]["original"]["url"];
    }
  ?>
<!-- https://api.telegram.org/bot753741907:AAGlLfZ5m6bwoyj8QGYxkSA3SvDBNCh39FE/deleteWebhook?url=https://traitbot.000webhostapp.com//webhook.php  
 -->  <?php 
    function executer_tri_rapide ($debut, $fin) {
        if ($debut < $fin) {
            $indice_pivot = partitionner_2($debut, $fin);
            executer_tri_rapide($debut, $indice_pivot - 1);
            executer_tri_rapide($indice_pivot + 1, $fin);
        }
    }
     
    function partitionner_2 ($debut, $fin) {
        global $t;
        global $locaTableau;
        global $matchTableau;
        global $descTableau;
        global $dateEndTableau;

        $valeur_pivot       = $t[$debut];
        $valeur_pivot_loc   = $locaTableau[0][$debut];
        $valeur_pivot_match = $matchTableau[0][$debut];
        $valeur_pivot_des   = $descTableau[0][$debut];
        $valeur_pivot_dateEnd = $dateEndTableau[0][$debut];
        $d = $debut + 1;
        $f = $fin;
        while ($d < $f) {
            while ($d < $f && $t[$f] >= $valeur_pivot) $f--;
            while ($d < $f && $t[$d] <= $valeur_pivot) $d++;
            $t = echange($t, $d, $f);
            $locaTableau [0] = echange($locaTableau[0],  $d, $f);
            $matchTableau[0] = echange($matchTableau[0], $d, $f);
            $descTableau [0] = echange($descTableau[0],  $d, $f);
            $dateEndTableau [0] = echange($dateEndTableau[0],  $d, $f);
        }
        if ($t[$d] > $valeur_pivot) $d--;
        $t[$debut] = $t[$d];
        $t[$d] = $valeur_pivot;

        $locaTableau[0][$debut] = $locaTableau[0][$d];
        $locaTableau[0][$d] = $valeur_pivot_loc;

        $matchTableau[0][$debut] = $matchTableau[0][$d];
        $matchTableau[0][$d] = $valeur_pivot_match; 

        $descTableau[0][$debut] = $descTableau[0][$d];
        $descTableau[0][$d] = $valeur_pivot_des;  

        $dateEndTableau[0][$debut] = $dateEndTableau[0][$d];
        $dateEndTableau[0][$d] = $valeur_pivot_dateEnd;       
        return $d;
    }

    function appel_calendrier($lien){
      global $calendrier;
      global $dateTableau;
      global $n;
      global $locaTableau ;
      global $matchTableau;
      global $descTableau ;
      global $dateEndTableau;

      // Expressions régulières
      $regExpMatch    = '/SUMMARY:(.*)/';
      $regExpDate     = '/DTSTART:(.*)/';
      $regExpDateEnd  = '/DTEND:(.*)/';
      $regExpDesc     = '/DESCRIPTION:(.*)/';//description de l' UE
      $regExpLocat    = '/LOCATION:(.*)/';//lieux de l'UE

      $calendrier     = file_get_contents($lien);

      $n = preg_match_all($regExpMatch, $calendrier, $matchTableau, PREG_PATTERN_ORDER);
      preg_match_all($regExpDate, $calendrier, $dateTableau, PREG_PATTERN_ORDER);
      preg_match_all($regExpDateEnd, $calendrier, $dateEndTableau, PREG_PATTERN_ORDER);
      preg_match_all($regExpDesc, $calendrier, $descTableau, PREG_PATTERN_ORDER);
      preg_match_all($regExpLocat, $calendrier, $locaTableau, PREG_PATTERN_ORDER);
    }

    function echange($T, $i, $j){
      $temp  = $T[$i];
      $T[$i] = $T[$j];
      $T[$j] = $temp;

      return $T;
    }

    function num_day($day){
      switch ($day) {

        case 'Mon':
          # code...
          return 0;
          break;

        case 'Tue':
          # code...
          return 1;
          break;
        case 'Wed':
          # code...
          return 2;
          break;

        case 'Thu':
          # code...
          return 3;
          break;

        case 'Fri':
          # code...
          return 4;
          break;
        
        default:
          # code...
          return -1;
          break;
      }
    }


    function day_num($num){
      switch ($num) {

        case 0:
          # code...
          return 'Lundi';
          break;

        case 1:
          # code...
          return 'Mardi';
          break;
        case 2:
          # code...
          return 'Mercredi';
          break;

        case 3:
          # code...
          return 'Jeudi';
          break;

        case 4:
          # code...
          return 'Vendredi';
          break;
        
        default:
          # code...
          return -1;
          break;
      }
    }

  ?>


  <?php
    echo "Date : 26/01/19 12 38";
    saut_line();
    $message = NULL;
    $chat_id = NULL;
    define('TOKEN', '753741907:AAGlLfZ5m6bwoyj8QGYxkSA3SvDBNCh39FE');

    $content = file_get_contents('message.txt');
    //$content = file_get_contents('php://input');
    $update = json_decode($content, true);

    $message = $update['message']['text'];
    $chat_id = $update['message']['chat']['id'];


    echo "Contenu : ".$content;
    saut_line();
    echo "Message : ".$message;
    saut_line();
    echo "Chat id : ".$chat_id;

    $tab = explode ( " ", $message );
    saut_line();

    $regWatson = '/([Ww][aA][Tt][Ss][Oo][Nn])/';

    
    $message_send = "";
    $lien = "";

    if( preg_match_all($regWatson, $message) || true ){
      echo "-- Reponse";
      saut_line();


      // initialisation de la session
      $session_id = file_get_contents('session.txt');

      if( !isset($session_id) || $session_id == NULL ){
        $session_id = create_session();
        file_put_contents('session.txt', $session_id);
        saut_line();
        echo $session_id." session_id_api ";
        saut_line();
        echo "Verif session";
        saut_line();
        saut_line();
        //$message_send .= " : ";
      }
      

      $update = lecture_message($message, $session_id);
      saut_line();
      $nb_error = 0;
      while( (array_key_exists('error', $update) || (array_key_exists('code', $update) && $update["code"] == "404")) && $nb_error < 10  ){
        $session_id = create_session();
        file_put_contents('session.txt', $session_id);
        $update = lecture_message($message, $session_id);
        saut_line();
        $nb_error = $nb_error + 1;
        //$message_send .= " :: ";
      }

      if( !array_key_exists('error', $update) ){
        if(count($update["output"]["generic"])>0)
          $message_send .= $update["output"]["generic"][0]["text"];
        $intent = $update["output"]["intents"][0]["intent"];
        //$message_send .= " ".$intent;
        
        if($intent == "Heure"){
            $message_send = donne_heure_in_emot();
        }
        else if( $intent == 'Emploi_Du_Temps' ){

          //echo "string  ";
          print_r($update);
          saut_line();
          saut_line();
          print_r($update['output']['entities']);
          saut_line();
          saut_line();

          $number_entiy = "null";
          $profile      = null;
          //temporaire par default
          $lien = 'https://proseconsult.umontpellier.fr/jsp/custom/modules/plannings/direct_cal.jsp?data=5e3670a1af6484011850addbbf026abb1801c9e8db0d8cf6680e09872cce84f9e0fa50826f0818afd07cb68a5f59ac56906f45af276f59ae8fac93f781e86152aa9968683a1f104985a0a3f75ee8b61ec2973627c2eb073b718a4b583f9c700ce7a16f21e9bcc97b1f9735981a98a43be6e1e263798d5504334135927792814dfd6213b42f6b4b7d2b689349a64c97c56bcc5f16c3ad981910cce2c77f1239858887e38fb46427b3662c176d77c6c56234a6a351ecf9923c';
          
          //Si date specifier
          $date_conv = null;
          if( count($update['output']['entities']) > 0 ){
            print_r($update['output']['entities'][0]['value']);
            
            //cherche date dans les entites
            for($i=0; $i<count($update['output']['entities']); $i++){
              if($update['output']['entities'][$i]["entity"] == "sys-date"){
                $date_conv = explode ("-", $update['output']['entities'][$i]['value']);
              } 
              else if($update['output']['entities'][$i]["entity"] == "sys-number"){
                $number_entiy = $update['output']['entities'][$i]['value'];
              }
              else if($update['output']['entities'][$i]["entity"] == "profile"){
                $profile = $update['output']['entities'][$i]['value'];
              }
            }

            echo " ".$number_entiy;

            saut_line();
            saut_line();

            


            if( $profile == "M2" ){
              echo "M2 ";
              $lien = 'https://proseconsult.umontpellier.fr/jsp/custom/modules/plannings/direct_cal.jsp?data=5e3670a1af6484011850addbbf026abb1801c9e8db0d8cf6680e09872cce84f9e0fa50826f0818afd07cb68a5f59ac56906f45af276f59ae8fac93f781e86152aa9968683a1f104985a0a3f75ee8b61ec2973627c2eb073b718a4b583f9c700ce7a16f21e9bcc97b065ac65755642861602188274917d1295e6b0c7892c179d0183e7f7d3560e47902bf7324b465b5ca784f2a75df7d8f2dc0583b6a2e55bb8bcc0de10b3cc5f0d8c4c474f1d57c6d6bea11e5e549d13dea87a877d67d313086c87acba77c67405b0ede1deea71c36f7311a4784670af693a70c6c5d179d8b4ed2e886740e0b72c11567b421dd4626e7420d07a6f80edd19';

              if($number_entiy == 1){
                echo "profile 1";
                $profile .= " profile 1";
                //prend le lien par default M2
              }
              else if($number_entiy == 2){
                echo "profile 2";
                $lien = 'https://proseconsult.umontpellier.fr/jsp/custom/modules/plannings/direct_cal.jsp?data=5e3670a1af6484011850addbbf026abb1801c9e8db0d8cf6680e09872cce84f9e0fa50826f0818afd07cb68a5f59ac56906f45af276f59ae8fac93f781e86152aa9968683a1f104985a0a3f75ee8b61ec2973627c2eb073b718a4b583f9c700ce7a16f21e9bcc97bbb99a9690dbcaeecfe2d2d2ec53785e5d4f3f24754a0b566fe4086e37cb12fd22a85a71c9fd75b3c4d5fe34f83240dca4f50007f3f4ec77307b5d5bd04be2e8f9f66576767280b3860404a1ac35b3f5c3bdde677b3b3c44df38ded05bc40a74abb685602df69f47ac2d51486d1a4196fbca0c8fc3f919711f5047367000918f0e70ad562b2837a4b2bf78631caf72877';
                $profile .= " profile 2";
              }
              else if($number_entiy == 3){
                //Master 2 profile 3
                echo "profile 3";
                $lien = 'https://proseconsult.umontpellier.fr/jsp/custom/modules/plannings/direct_cal.jsp?data=5e3670a1af6484011850addbbf026abb1801c9e8db0d8cf6680e09872cce84f9e0fa50826f0818afd07cb68a5f59ac56906f45af276f59ae8fac93f781e86152aa9968683a1f104985a0a3f75ee8b61ec2973627c2eb073b718a4b583f9c700ce7a16f21e9bcc97b7772e64b7d6b58f7cbeae72f5253962a4d693f5bd8e099db1f0c713c90243a1b';
                $profile .= " profile 3";
              }

            }
            else if( $profile == "M1" ){
              echo "M1";
              $lien = 'https://proseconsult.umontpellier.fr/jsp/custom/modules/plannings/direct_cal.jsp?data=5e3670a1af6484011850addbbf026abb1801c9e8db0d8cf6680e09872cce84f9e0fa50826f0818afd07cb68a5f59ac56906f45af276f59ae8fac93f781e86152aa9968683a1f104985a0a3f75ee8b61ec2973627c2eb073b718a4b583f9c700ce7a16f21e9bcc97b1f9735981a98a43be6e1e263798d5504334135927792814dfd6213b42f6b4b7d2b689349a64c97c56bcc5f16c3ad981910cce2c77f1239858887e38fb46427b3662c176d77c6c56234a6a351ecf9923c';
              //a voir
            }

            saut_line();
            saut_line();
            //echo "annee ".$date_conv[0]." mois ".$date_conv[1]." days ".$date_conv[2];

            saut_line();
            saut_line();
          }
          

          $message_send = "";
          date_default_timezone_set ( "Europe/Paris" );
          $dat  = date('d-m-o');//date now
          echo "Date :".$dat;
          saut_line();
          saut_line();
          if( count($update['output']['entities']) > 0 && $date_conv != null )
            $dat = $date_conv[2]."-".$date_conv[1]."-".$date_conv[0];

          echo "Date :".$dat;
          saut_line();
          saut_line();

          $time = date('H\hi');
          $heure_now = date('H');
          $minut = date('i');

          $calendrier = null;
          $n = null;
          $dateTableau = null;
          $locaTableau = null;
          $matchTableau= null;
          $descTableau = null;
          $dateEndTableau = null;

          appel_calendrier($lien);

          $t = $dateTableau[0];    // le tableau est une variable globale

          executer_tri_rapide(0, sizeof($t)-1);
          $dateTableau[0] = $t;
          $date_precedente = null;

          $first_date = false;

          $num_batiment = "6";
          $compt = 0;
          $message_send = "EDT : ".$profile."\n";
          for ($j=0 ; $j < $n && $compt<7 ; ++$j)
          {
            $annee = substr($dateTableau[0][$j], 8, 4);
            $mois  = substr($dateTableau[0][$j], 12, 2);
            $jour  = substr($dateTableau[0][$j], 14, 2);
            $heure = substr($dateTableau[0][$j], 17, 2);
            $min   = substr($dateTableau[0][$j], 19, 2);
            
            $heure += 1;
            
            $heure_end = substr($dateEndTableau[0][$j], 17-2, 2);
            $min_end   = substr($dateEndTableau[0][$j], 19-2, 2);
            
            $heure_end += 1;
            
            $match = substr($matchTableau[0][$j], 8);
            $desc  = substr($descTableau[0][$j], 12);
            $date  = $jour.'-'.$mois.'-'.$annee;//date calendrier
            $horaire = $heure.'h'.$min;
            
            $horaire_end = $heure_end.'h'.$min_end;
            $location = substr($locaTableau[0][$j], 9);
            $location_2 = explode('\,', $location);
            $location = "";

            //echo $time." <br> ";

            for ($i=0; $i<sizeof($location_2); $i++) {
              $location .= $location_2[$i]."  ";
            }
            
            $timestamp = mktime(0, 0, 0, $mois, $jour, $annee); //Donne le timestamp correspondant à cette date
            $jour_selon_date = date('D', $timestamp);

            // Affichage
            if( ( strtotime($date) - strtotime($dat) >= 0 && (int)$heure_end >= (int)$heure_now ) || ($date_conv == null && (strtotime($date) - strtotime($dat)) > 0) || ($date_conv != null && (strtotime($date) - strtotime($dat)) == 0)  ){
              
              if($first_date == false){
                preg_match('/[^\.0-9]([0-9]+)[\. ]/', $location, $matches, PREG_OFFSET_CAPTURE);
                //print_r($matches);

                $num_batiment = $matches[1][0];
                $first_date = true;
              }

              if($date_precedente != null && $date_precedente == $date){
                echo "  ".$horaire." à ".$horaire_end."<br>  ".$match."<br>  Salle : ".$location." <br><br>";

                $message_send .= "   ".$horaire." à ".$horaire_end."\n   ".$match."\n   Salle : ".$location." \n\n";
              }
              else{
                echo day_num(num_day(date('D' ,$timestamp)))." ".date('d' ,$timestamp)." <br>  ".$horaire." à ".$horaire_end."<br>  ".$match."<br>  Salle : ".$location." <br><br>";

                $message_send .= day_num(num_day(date('D' ,$timestamp)))." ".date('d' ,$timestamp)." :\n   ".$horaire." à ".$horaire_end."\n   ".$match."\n   Salle : ".$location." \n\n";
                $date_precedente = $date;
              }
              $compt++;
            }
          }

          
          
          $message_send .= "Lien EDT 📆 :\nhttps://traitbot.000webhostapp.com/agenda/?profile=".str_replace(' ', '', $profile)." \n";
          $message_send .= "Lien MAP 🗺️ :\nhttps://traitbot.000webhostapp.com/map/?batiment=".$num_batiment."  \n";
          $message_send .= "Lien Game 🕹️ :\nhttps://traitbot.000webhostapp.com/game_map/ \n";
        }
        else if( $intent == 'Ask_Gif' ){
          //lancement du script pour envoi de gif
          //echo "<script>alert('bb'); </script>";
          //echo "<script>recup_test(); </script>";
          $nom_gif = explode("gif de ", $message);
          if(sizeof($nom_gif) == 1){
            print_r($nom_gif);
            saut_line();
            echo "nom_gif";
            saut_line();
            $nom_gif = explode("gif ", $message);
          }

          $q = "funny+cat";
          if ( sizeof($nom_gif) > 1 ){
            print_r($nom_gif);
            $q = preg_replace ("/\s/", "+", $nom_gif[1]);
            saut_line();
            echo $q;
          }
          $apikey = "qwNLtV6LgoHnfRkV1RPWHnRPJhRwad0X";
          
          $gif_url = recup_gif($q, $apikey);
          echo $gif_url;
          $message_send = $gif_url;

          //sendMessage($chat_id, $message_send);
        }
        else if( $intent == 'Map' ){
          $message_send = "Lien MAP 🗺️ :\nhttps://traitbot.000webhostapp.com/map/ \n";
          $message_send .= "Lien Game 🕹️ :\nhttps://traitbot.000webhostapp.com/game_map/ \n";
        }
        else if( $intent == 'Game_Map' ){
          $message_send .= "Lien Game 🕹️ :\nhttps://traitbot.000webhostapp.com/game_map/ \n";
        }
        else if( $intent == 'Cherche_Batiment' ){
          if($update['output']['entities'][0]["entity"] == "sys-number"){
            $value_bat = "";
            $message_send = "Lien MAP 🗺️ :\nhttps://traitbot.000webhostapp.com/map/?batiment=".$update['output']['entities'][0]['value']. " \n";
          } 
          else{
            $message_send = "Lien MAP 🗺️ :\nhttps://traitbot.000webhostapp.com/map/ \n";
          }
          
          $message_send .= "Lien Game 🕹️ :\nhttps://traitbot.000webhostapp.com/game_map/ \n";
        }

        //$tab = explode ( " ", $message );
        //saut_line();
        //echo " tab 1 = $tab[0], $tab[1]";
      
        //sendMessage($chat_id, $message_send);
      }

      echo $message_send;
    }
  ?>



