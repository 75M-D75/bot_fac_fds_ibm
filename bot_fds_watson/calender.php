<!doctype html>
<html lang='fr'>
<head>
  <meta charset='utf-8'>
  <title>WatsoneBot</title>
</head>
<body>
  <div>
  <p>

  <?php 

    function executer_tri_rapide ($debut, $fin) {
        if ($debut < $fin) {
            $indice_pivot = partitionner_2($debut, $fin);
            executer_tri_rapide($debut, $indice_pivot - 1);
            executer_tri_rapide($indice_pivot + 1, $fin);
        }
    }
     
    function partitionner_2 ($debut, $fin) {
        global $t;
        global $locaTableau;
        global $matchTableau;
        global $descTableau;
        global $dateEndTableau;

        $valeur_pivot       = $t[$debut];
        $valeur_pivot_loc   = $locaTableau[0][$debut];
        $valeur_pivot_match = $matchTableau[0][$debut];
        $valeur_pivot_des   = $descTableau[0][$debut];
        $valeur_pivot_dateEnd = $dateEndTableau[0][$debut];
        $d = $debut + 1;
        $f = $fin;
        while ($d < $f) {
            while ($d < $f && $t[$f] >= $valeur_pivot) $f--;
            while ($d < $f && $t[$d] <= $valeur_pivot) $d++;
            $t = echange($t, $d, $f);
            $locaTableau [0] = echange($locaTableau[0],  $d, $f);
            $matchTableau[0] = echange($matchTableau[0], $d, $f);
            $descTableau [0] = echange($descTableau[0],  $d, $f);
            $dateEndTableau [0] = echange($dateEndTableau[0],  $d, $f);
        }
        if ($t[$d] > $valeur_pivot) $d--;
        $t[$debut] = $t[$d];
        $t[$d] = $valeur_pivot;

        $locaTableau[0][$debut] = $locaTableau[0][$d];
        $locaTableau[0][$d] = $valeur_pivot_loc;

        $matchTableau[0][$debut] = $matchTableau[0][$d];
        $matchTableau[0][$d] = $valeur_pivot_match; 

        $descTableau[0][$debut] = $descTableau[0][$d];
        $descTableau[0][$d] = $valeur_pivot_des;  

        $dateEndTableau[0][$debut] = $dateEndTableau[0][$d];
        $dateEndTableau[0][$d] = $valeur_pivot_dateEnd;       
        return $d;
    }

    function appel_calendrier(){
      global $calendrier;
      global $dateTableau;
      global $n;
      global $locaTableau ;
      global $matchTableau;
      global $descTableau ;
      global $dateEndTableau;

      // Expressions régulières
      $regExpMatch = '/SUMMARY:(.*)/';
      $regExpDate  = '/DTSTART:(.*)/';
      $regExpDateEnd = '/DTEND:(.*)/';
      $regExpDesc  = '/DESCRIPTION:(.*)/';
      $regExpLocat = '/LOCATION:(.*)/';

      $calendrier = file_get_contents('https://proseconsult.umontpellier.fr/jsp/custom/modules/plannings/direct_cal.jsp?data=5e3670a1af6484011850addbbf026abb1801c9e8db0d8cf6680e09872cce84f9e0fa50826f0818af16cfc8af7aef7fd1906f45af276f59aec18424f8595af9f9a6b28cb855546dc71e4c0e6e6a459e378d3f4109b6629391');

      /*echo "<br>";
      echo $calendrier;
      echo "<br>";*/
      //print_r($dateTableau);

      $n = preg_match_all($regExpMatch, $calendrier, $matchTableau, PREG_PATTERN_ORDER);
      preg_match_all($regExpDate, $calendrier, $dateTableau, PREG_PATTERN_ORDER);
      preg_match_all($regExpDateEnd, $calendrier, $dateEndTableau, PREG_PATTERN_ORDER);
      preg_match_all($regExpDesc, $calendrier, $descTableau, PREG_PATTERN_ORDER);
      preg_match_all($regExpLocat, $calendrier, $locaTableau, PREG_PATTERN_ORDER);
      //print_r($dateTableau);

      //$min = substr($dateTableau[0][0], 19, 2);
      //echo " ------ ".substr($dateTableau[0][0], 17, 2)."  ".$min."   ".$dateTableau[0][0];
    }

    function echange($T, $i, $j){
      $temp  = $T[$i];
      $T[$i] = $T[$j];
      $T[$j] = $temp;

      return $T;
    }

    function num_day($day){
      switch ($day) {

        case 'Mon':
          # code...
          return 0;
          break;

        case 'Tue':
          # code...
          return 1;
          break;
        case 'Wed':
          # code...
          return 2;
          break;

        case 'Thu':
          # code...
          return 3;
          break;

        case 'Fri':
          # code...
          return 4;
          break;
        
        default:
          # code...
          return -1;
          break;
      }
    }


    function day_num($num){
      switch ($num) {

        case 0:
          # code...
          return 'Lundi';
          break;

        case 1:
          # code...
          return 'Mardi';
          break;
        case 2:
          # code...
          return 'Mercredi';
          break;

        case 3:
          # code...
          return 'Jeudi';
          break;

        case 4:
          # code...
          return 'Vendredi';
          break;
        
        default:
          # code...
          return -1;
          break;
      }
    }

    /*function date_inferieur($date_1, $date_2){
      $annee_1 = substr($date_1, 8, 4);
      $mois_1  = substr($date_1, 12, 2);
      $jour_1  = substr($date_1, 14, 2);

      $annee_2 = substr($date_2, 8, 4);
      $mois_2  = substr($date_2, 12, 2);
      $jour_2  = substr($date_2, 14, 2);
      echo $jour_1.'/'.$mois_1.'/'.$annee_1." --<br>";
      echo $jour_2.'/'.$mois_2.'/'.$annee_2." --<br>";

      if( $annee_1 < $annee_2 ){
        return -1;
      }
      else if($annee_1 == $annee_2){
        if( $mois_1 < $mois_2 ){
          return -1;
        }
        else if( $mois_1 == $mois_2 ){
          if( $jour_1 < $jour_2 ){
            return -1;
          }
          else if( $jour_1 == $jour_2 ){
            return 0;
          }
          else{
            return 1;
          }
        }
        else{
          return 1;
        }
      }
      else{
        return 1;;
      }
    }*/

    date_default_timezone_set ( "Europe/Paris" );
    $dat  = date('d/m/o');
    $time = date('H\hi');
    $heure_now = date('H');
    $minut = date('i');

    $calendrier = null;
    $n = null;
    $dateTableau = null;
    $locaTableau = null;
    $matchTableau= null;
    $descTableau = null;
    $dateEndTableau = null;

    appel_calendrier();

    $t = $dateTableau[0];    // le tableau est une variable globale

    executer_tri_rapide(0, sizeof($t)-1);
    $dateTableau[0] = $t;

    $compt = 0;
    $bool = false;
    $date_precedente = null;
    for ($j=0 ; $j < $n && $compt<7 && !$bool ; ++$j)
    {
      $annee = substr($dateTableau[0][$j], 8, 4);
      $mois  = substr($dateTableau[0][$j], 12, 2);
      $jour  = substr($dateTableau[0][$j], 14, 2);
      $heure = substr($dateTableau[0][$j], 17, 2);
      $min   = substr($dateTableau[0][$j], 19, 2);

      $heure += 1;

      $heure_end = substr($dateEndTableau[0][$j], 17-2, 2);
      $min_end   = substr($dateEndTableau[0][$j], 19-2, 2);

      $heure_end += 1;

      $match = substr($matchTableau[0][$j], 8);
      $desc  = substr($descTableau[0][$j], 12);
      $date  = $jour.'-'.$mois.'-'.$annee;
      $horaire = $heure.'h'.$min;

      $horaire_end = $heure_end.'h'.$min_end;
      $location = substr($locaTableau[0][$j], 9);
      $location_2 = explode('\,', $location);
      $location = "";

      //echo $time." <br> ";

      for ($i=0; $i<sizeof($location_2); $i++) {
        $location .= $location_2[$i]."  ";
      }
       
      $timestamp = mktime(0, 0, 0, $mois, $jour, $annee); //Donne le timestamp correspondant à cette date
      $jour_selon_date = date('D', $timestamp);
       
      //echo date('D', $timestamp)."<br>";
      if( $jour_selon_date == "Sat"){
        $bool = true;
        echo "true <br>";
      }

      // Affichage
      if( ( strtotime($date) - strtotime($dat) >= 0 && (int)$heure >= (int)$heure_now ) || (strtotime($date) - strtotime($dat)) > 0  ){
        
        if($date_precedente != null && $date_precedente == $date){
          echo $horaire." à ".$horaire_end."<br> ".$match."<br> Salle : ".$location." <br><br>";
        }
        else{
          echo day_num(num_day(date('D' ,$timestamp)))." ".date('d' ,$timestamp)." <br> ".$horaire." à ".$horaire_end."<br> ".$match."<br> Salle : ".$location." <br><br>";
          $date_precedente = $date;
        }

        $compt++;
      }
    }
    //35b88b5c4be09500c630111c4a10f450
  ?>



  </p>
  </div>
</body>
</html>


